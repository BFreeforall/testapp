<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>workshop1_4</title>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

</head>
<body>
    <form action="workshop1_view.php" method="GET">
        First Name (*) : <input type="text" id="firstname" name="firstname">
        Last Name (*) : <input type="text" id="lastname" name="lastname">
        <input type="submit" value="Save">
    </form>
</body>

    <script>
        $(document).ready(function () {
            $('form').submit(function (e) {
                if (!Validation()) {
                    return false;
                }
            });

        });

        function Validation(){
            var v_firstname = $('#firstname');
            var v_lastname = $('#lastname');
            if (v_firstname.val()==""){
                alert('Please input firstname'); v_firstname.focus();
                return false;
            }
            if (v_lastname.val()==""){
                alert('Please input lastname'); v_lastname.focus();
                return false;
            }
            return true;
        }

    </script>
</html>
