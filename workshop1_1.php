<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="workshop1_view.php" method="POST">
        First Name (*) : <input type="text" id="firstname" name="firstname">
        Last Name (*) : <input type="text" id="lastname" name="lastname">
        <input type="submit" value="Save" onClick="return Validation();">
    </form>
</body>
    <script>

        function Validation(){
            var v_firstname = document.getElementById('firstname');
            var v_lastname = document.getElementById('lastname');
            if (v_firstname.value=="") {
                alert('Please input firstname'); v_firstname.focus();
                return false;
            }
            if (v_lastname.value=="") {
                alert('Please input lastname'); v_lastname.focus();
                return false;
            }
            return true;
        }

    </script>
</html>