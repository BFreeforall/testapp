<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>workshop1_6</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
    <form action="workshop1_view_json.php" method="GET">
        First Name (*) : <input type="text" id="firstname" name="firstname" placeholder="กรุณากรอกชื่อ">
        Last Name (*) : <input type="text" id="lastname" name="lastname" placeholder="กรุณากรอกนามสกุล">
        <input type="submit" value="Save">
        <input type="reset" value"Clear">
    </form>
</body>

    <script>
        $(document).ready(function () {
            
            $('form').submit(function (e) {
                e.preventDefault();

                if (Validation()) {
                    $.ajax({
                        type: "GET",
                        dataType: "JSON",
                        url: $(this).attr('action'),
                        data: $(this).serialize(),
                        success: function(response){
                            // alert("First Name: "+response.firstname+" & Last Name: "+response.lastname);
                            console.log(response);
                            // $('input[type=submit]').hide();
                            $('input[type=submit]').attr('disabled','true');
                            $('#firstname').addClass('firstname');
                        }
                    });
                    // return false;
                }
            });

            $('input[type=reset]').click(function(){
                $('input[type=submit]').removeAttr('disabled','true');
                $('#firstname').removeClass('firstname');
            });

        });

        function Validation(){
            $('input').each(function (index, element) {
                if ($.trim($(this).val())=="") {
                    alert('Plase input ' + $(this).attr('placeholder'));
                    return false;
                }
            });
            return true;
        }

    </script>
</html>
