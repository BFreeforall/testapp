<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>workshop1_5</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
    <form action="workshop1_view.php" method="GET">
        First Name (*) : <input type="text" id="firstname" name="firstname">
        Last Name (*) : <input type="text" id="lastname" name="lastname">
        <input type="submit" value="Save">
    </form>
</body>

    <script>
        $(document).ready(function () {
            $('form').submit(function (e) {
                e.preventDefault();

                if (Validation()) {
                    $.ajax({
                        type: "GET",
                        url: $(this).attr('action'),
                        data: $(this).serialize(),
                        success: function(response){
                            alert(response);
                        }
                    });
                    // return false;
                }
            });

        });

        function Validation(){
            var v_firstname = $('#firstname');
            var v_lastname = $('#lastname');
            if (v_firstname.val()==""){
                alert('Please input firstname'); v_firstname.focus();
                return false;
            }
            if (v_lastname.val()==""){
                alert('Please input lastname'); v_lastname.focus();
                return false;
            }
            return true;
        }

    </script>
</html>
